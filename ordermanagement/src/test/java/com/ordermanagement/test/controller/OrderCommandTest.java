package com.ordermanagement.test.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ordermanagement.ordermanagement.commandservice.OrderCommandService;
import com.ordermanagement.ordermanagement.controller.OrderController;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.dto.OrderingDTO;
import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;
import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;
import com.ordermanagement.ordermanagement.model.Order;
import com.ordermanagement.ordermanagement.queryService.OrderQueryService;
import com.ordermanagement.ordermanagement.repository.OrderRepository;
import com.ordermanagement.ordermanagement.request.RequestDeleteOrder;
import com.ordermanagement.ordermanagement.request.RequestUpdateOrder;
import com.ordermanagement.ordermanagement.response.ResponseOrder;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

public class OrderCommandTest extends BaseTest {
	public OrderCommandTest() {
	}

	private OrderCommandService orderCommandService;
	private OrderRepository orderRepository;

	@Before
	public void setup() {

		orderRepository = ApplicationUtils.createMock(OrderRepository.class);
		orderCommandService = new OrderCommandService(orderRepository);
	}

	@Test
	public void updateOrder() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(3);
		updateOrderDTO.setOrderDate(LocalDate.now().plusDays(2));
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Order order = new Order();
		order.setId(1L);
		order.setStatus(OrderStatusEnum.DELIVERED);
		order.setOrderCount(3);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Mockito.when(orderRepository.getOne(Mockito.any())).thenReturn(order);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		OrderDTO orderDTO = orderCommandService.updateOrder(updateOrderDTO);
		assertThat(orderDTO).isNotNull();
		assertThat(orderDTO.getOrderCount()).isEqualTo(3);
	}
	@Test
	public void ordering() throws Exception {
		OrderingDTO orderingDTO = new OrderingDTO();
		orderingDTO.setOrderCount(3);
		orderingDTO.setOrderDate(LocalDate.now().plusDays(2));
		orderingDTO.setUserId(1L);
		orderingDTO.setProductId(1L);
		orderingDTO.setCustomerId(1L);
		Order order = new Order();
		order.setId(1L);
		order.setStatus(OrderStatusEnum.DELIVERED);
		order.setOrderCount(3);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		Mockito.when(orderRepository.getOne(Mockito.any())).thenReturn(order);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		OrderDTO orderDTO = orderCommandService.ordering(orderingDTO);
		assertThat(orderDTO).isNotNull();
		assertThat(orderDTO.getOrderCount()).isEqualTo(3);
	}
	@Test(expected = Exception.class)
	public void updateOrderCountLowerthan1Exeption() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(0);
		updateOrderDTO.setOrderDate(LocalDate.now().plusDays(2));
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Order order = new Order();
		order.setId(1L);
		order.setStatus(OrderStatusEnum.DELIVERED);
		order.setOrderCount(3);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Mockito.when(orderRepository.getOne(Mockito.any())).thenReturn(order);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		OrderDTO orderDTO = orderCommandService.updateOrder(updateOrderDTO);
		assertThat(orderDTO).isNotNull();
		assertThat(orderDTO.getOrderCount()).isEqualTo(3);
	}
	@Test(expected = Exception.class)
	public void updateOrderOrderDatePastDateExeption() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(0);
		updateOrderDTO.setOrderDate(LocalDate.now().minusDays(2));
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Order order = new Order();
		order.setId(1L);
		order.setStatus(OrderStatusEnum.DELIVERED);
		order.setOrderCount(3);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Mockito.when(orderRepository.getOne(Mockito.any())).thenReturn(order);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		OrderDTO orderDTO = orderCommandService.updateOrder(updateOrderDTO);
		assertThat(orderDTO).isNotNull();
		assertThat(orderDTO.getOrderCount()).isEqualTo(3);
	}
	@Test
	public void delete() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(3);
		updateOrderDTO.setOrderDate(LocalDate.now());
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Order order = new Order();
		order.setStatus(OrderStatusEnum.DELIVERED);
		order.setOrderCount(3);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Mockito.when(orderRepository.getOne(Mockito.any())).thenReturn(order);
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(order);
		orderCommandService.deleteOrder(1L);
	}
}
