package com.ordermanagement.test.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ordermanagement.ordermanagement.commandservice.OrderCommandService;
import com.ordermanagement.ordermanagement.controller.OrderController;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;
import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;
import com.ordermanagement.ordermanagement.model.Order;
import com.ordermanagement.ordermanagement.queryService.OrderQueryService;
import com.ordermanagement.ordermanagement.repository.OrderRepository;
import com.ordermanagement.ordermanagement.request.RequestDeleteOrder;
import com.ordermanagement.ordermanagement.request.RequestUpdateOrder;
import com.ordermanagement.ordermanagement.response.ResponseOrder;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

public class OrderQueryTest extends BaseTest {
	public OrderQueryTest() {
	}

	private OrderRepository orderRepository;
	private OrderQueryService orderQueryService;


	@Before
	public void setup() {

		orderRepository = ApplicationUtils.createMock(OrderRepository.class);
		orderQueryService = new OrderQueryService(orderRepository);
	}

	@Test
	public void getOrderByOrderIdTest() throws Exception {
		Order order = new Order();
		order.setId(1L);
		order.setStatus(OrderStatusEnum.TAKEN);
		order.setOrderCount(2);
		order.setOrderDate(LocalDate.now());
		order.setUserId(1L);
		order.setProductId(1L);
		order.setCustomerId(1L);
		Mockito.when( orderRepository.getOne(Mockito.anyLong())).thenReturn(order);
		OrderDTO response = orderQueryService.getOrderByOrderId(1L);
		assertThat(response.getCustomerId()).isEqualTo(order.getCustomerId());
	}

	@Test
	public void getOrdersByUserIdWithResult() throws Exception {
		Order mockOrder = new Order();
		mockOrder.setId(1L);
		mockOrder.setStatus(OrderStatusEnum.TAKEN);
		mockOrder.setOrderCount(2);
		mockOrder.setOrderDate(LocalDate.now());
		mockOrder.setUserId(1L);
		mockOrder.setProductId(1L);
		mockOrder.setCustomerId(1L);
		List<Order> mockList = new ArrayList<>();
		mockList.add(mockOrder);
		Mockito.when(orderRepository.findByUserId(1L)).thenReturn(mockList);
		List<OrderDTO> response = orderQueryService.getOrdersByUserId(1L);
		assertThat(response.get(0).getCustomerId()).isEqualTo(mockOrder.getCustomerId());
	}




}
