package com.ordermanagement.test.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.ordermanagement.ordermanagement.commandservice.OrderCommandService;
import com.ordermanagement.ordermanagement.controller.OrderController;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.dto.OrderingDTO;
import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;
import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;
import com.ordermanagement.ordermanagement.queryService.OrderQueryService;
import com.ordermanagement.ordermanagement.request.RequestDeleteOrder;
import com.ordermanagement.ordermanagement.request.RequestOrdering;
import com.ordermanagement.ordermanagement.request.RequestUpdateOrder;
import com.ordermanagement.ordermanagement.response.ResponseOrder;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

public class OrderControllerTest extends BaseTest {
	public OrderControllerTest() {
	}

	private OrderCommandService orderCommandService;
	private OrderQueryService orderQueryService;
	private OrderController controller;

	@Before
	public void setup() {

		orderQueryService = ApplicationUtils.createMock(OrderQueryService.class);
		orderCommandService = ApplicationUtils.createMock(OrderCommandService.class);
		controller = new OrderController(orderCommandService, orderQueryService);
	}

	@Test
	public void getOrderByOrderIdTest() throws Exception {
		OrderDTO mockOrderDTO = new OrderDTO();
		mockOrderDTO.setId(1L);
		mockOrderDTO.setStatus(OrderStatusEnum.TAKEN);
		mockOrderDTO.setOrderCount(2);
		mockOrderDTO.setOrderDate(LocalDate.now());
		mockOrderDTO.setUserId(1L);
		mockOrderDTO.setProductId(1L);
		mockOrderDTO.setCustomerId(1L);
		Mockito.when(orderQueryService.getOrderByOrderId(1L)).thenReturn(mockOrderDTO);
		ResponseOrder response = controller.getOrderByOrderId(1L);
		assertThat(response.getOrderDTOList().get(0).getCustomerId()).isEqualTo(mockOrderDTO.getCustomerId());
	}

	@Test
	public void getOrdersByUserIdWithResult() throws Exception {
		OrderDTO mockOrderDTO = new OrderDTO();
		mockOrderDTO.setId(1L);
		mockOrderDTO.setStatus(OrderStatusEnum.TAKEN);
		mockOrderDTO.setOrderCount(2);
		mockOrderDTO.setOrderDate(LocalDate.now());
		mockOrderDTO.setUserId(1L);
		mockOrderDTO.setProductId(1L);
		mockOrderDTO.setCustomerId(1L);
		List<OrderDTO> mockList = new ArrayList<>();
		mockList.add(mockOrderDTO);
		Mockito.when(orderQueryService.getOrdersByUserId(1L)).thenReturn(mockList);
		ResponseOrder response = controller.getOrdersByUserId(1L);
		assertThat(response.getOrderDTOList().get(0).getCustomerId()).isEqualTo(mockOrderDTO.getCustomerId());
	}

	@Test
	public void getOrdersByUserIdwithNoResult() throws Exception {
		Mockito.when(orderQueryService.getOrdersByUserId(1L)).thenReturn(null);
		ResponseOrder response = controller.getOrdersByUserId(1L);
		assertThat(response).isNotNull();
		assertThat(response.getOrderDTOList()).isNull();
	}

	@Test
	public void getOrdersByOrderIdwithNoResult() throws Exception {
		Mockito.when(orderQueryService.getOrderByOrderId(1L)).thenReturn(null);
		ResponseOrder response = controller.getOrderByOrderId(1L);
		assertThat(response).isNotNull();
		assertThat(response.getOrderDTOList()).isNull();
	}
	
	@Test
	public void ordering() throws Exception {
		OrderingDTO orderingDTO=new OrderingDTO();
		orderingDTO.setCustomerId(1L);
		orderingDTO.setProductId(1L);
		orderingDTO.setOrderCount(2);
		orderingDTO.setOrderDate(LocalDate.now());
		OrderDTO orderDTO=new OrderDTO();
		orderDTO.setId(1L);
		orderDTO.setOrderCount(2);
		orderDTO.setStatus(OrderStatusEnum.TAKEN);
		Mockito.when(orderCommandService.ordering(orderingDTO)).thenReturn(orderDTO);
		@Valid RequestOrdering request=new RequestOrdering();
		request.setOrderingDTO(orderingDTO);
		ResponseOrder response = controller.ordering(request);
		assertThat(response).isNotNull();
		assertThat(response.getOrderDTOList().get(0).getOrderCount()).isEqualTo(orderingDTO.getOrderCount());
	}

	@Test
	public void getOrdersByOrderIdwithResult() throws Exception {
		OrderDTO mockOrderDTO = new OrderDTO();
		mockOrderDTO.setId(1L);
		mockOrderDTO.setStatus(OrderStatusEnum.TAKEN);
		mockOrderDTO.setOrderCount(2);
		mockOrderDTO.setOrderDate(LocalDate.now());
		mockOrderDTO.setUserId(1L);
		mockOrderDTO.setProductId(1L);
		mockOrderDTO.setCustomerId(1L);
		Mockito.when(orderQueryService.getOrderByOrderId(1L)).thenReturn(mockOrderDTO);
		ResponseOrder response = controller.getOrderByOrderId(1L);
		assertThat(response).isNotNull();
		assertThat(response.getOrderDTOList().get(0).getId()).isEqualTo(1L);
	}

	@Test
	public void updateOrder() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(3);
		updateOrderDTO.setOrderDate(LocalDate.now());
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setId(1L);
		orderDTO.setStatus(OrderStatusEnum.DELIVERED);
		orderDTO.setOrderCount(3);
		orderDTO.setOrderDate(LocalDate.now());
		orderDTO.setUserId(1L);
		orderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		Mockito.when(orderCommandService.updateOrder(updateOrderDTO)).thenReturn(orderDTO);
		RequestUpdateOrder request = new RequestUpdateOrder();
		request.setUpdateOrderDTO(updateOrderDTO);
		ResponseOrder response = controller.updateOrder(request);
		assertThat(response).isNotNull();
		assertThat(response.getOrderDTOList().get(0).getOrderCount()).isEqualTo(3);
	}
	@Test
	public void delete() throws Exception {
		UpdateOrderDTO updateOrderDTO = new UpdateOrderDTO();
		updateOrderDTO.setId(1L);
		updateOrderDTO.setStatus(OrderStatusEnum.DELIVERED);
		updateOrderDTO.setOrderCount(3);
		updateOrderDTO.setOrderDate(LocalDate.now());
		updateOrderDTO.setUserId(1L);
		updateOrderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		OrderDTO orderDTO = new OrderDTO();
		orderDTO.setId(1L);
		orderDTO.setStatus(OrderStatusEnum.DELIVERED);
		orderDTO.setOrderCount(3);
		orderDTO.setOrderDate(LocalDate.now());
		orderDTO.setUserId(1L);
		orderDTO.setProductId(1L);
		updateOrderDTO.setCustomerId(1L);
		@Valid RequestDeleteOrder request = new RequestDeleteOrder();
		request.setOrder(orderDTO);;
		ResponseOrder response = controller.deleteOrder(request);
		assertThat(response).isNotNull();
	}
}
