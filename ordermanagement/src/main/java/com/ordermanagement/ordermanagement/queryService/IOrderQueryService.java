package com.ordermanagement.ordermanagement.queryService;

import java.util.List;

import com.ordermanagement.ordermanagement.dto.OrderDTO;

public interface IOrderQueryService {

	List<OrderDTO> getOrdersByUserId(Long userId);

	OrderDTO getOrderByOrderId(Long orderId);

}
