package com.ordermanagement.ordermanagement.queryService;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.model.Order;
import com.ordermanagement.ordermanagement.repository.OrderRepository;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

@Service
public class OrderQueryService implements IOrderQueryService {
	private OrderRepository orderRepository;
	@Autowired
	public OrderQueryService(OrderRepository orderRepository) {
		this.orderRepository=orderRepository;
	}
	
	@Override
	public List<OrderDTO> getOrdersByUserId(Long userId) {
		List<OrderDTO> resultList=new ArrayList<>();
		List<Order> orderList =orderRepository.findByUserId(userId);
		if(!ApplicationUtils.isListEmptyOrNull(orderList)){
			for (Order item : orderList) {
			resultList.add(item.toDTO());
			}
		}
		return resultList;
		
	}

	@Override
	public OrderDTO getOrderByOrderId(Long orderId) {
		Order model = orderRepository.getOne(orderId);
		return model.toDTO();
	}

}
