package com.ordermanagement.ordermanagement.request;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.ordermanagement.ordermanagement.dto.DTO;

import io.swagger.annotations.ApiModel;


@ApiModel
@NotNull
@Valid
public class RequestDeleteOrder {
	@Valid @NotNull
private DTO order;

public DTO getOrder() {
	return order;
}

public void setOrder(DTO order) {
	this.order = order;
}



}
