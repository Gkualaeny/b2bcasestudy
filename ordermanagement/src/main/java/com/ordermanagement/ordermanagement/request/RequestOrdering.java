package com.ordermanagement.ordermanagement.request;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.ordermanagement.ordermanagement.dto.OrderingDTO;

import io.swagger.annotations.ApiModel;


@ApiModel
@Valid
@NotNull
public class RequestOrdering {
	@Valid
	@NotNull
private OrderingDTO orderingDTO;

public OrderingDTO getOrderingDTO() {
	return orderingDTO;
}

public void setOrderingDTO(OrderingDTO orderingDTO) {
	this.orderingDTO = orderingDTO;
}

}
