package com.ordermanagement.ordermanagement.request;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;

import io.swagger.annotations.ApiModel;


@ApiModel
@NotNull
@Valid
public class RequestUpdateOrder {
	@NotNull
	@Valid
private UpdateOrderDTO updateOrderDTO;

public UpdateOrderDTO getUpdateOrderDTO() {
	return updateOrderDTO;
}

public void setUpdateOrderDTO(UpdateOrderDTO updateOrderDTO) {
	this.updateOrderDTO = updateOrderDTO;
}


}
