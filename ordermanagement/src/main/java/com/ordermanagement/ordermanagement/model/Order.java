package com.ordermanagement.ordermanagement.model;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.ordermanagement.ordermanagement.constant.Constants;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

@Entity
@Table(name = "ordertable")
public class Order {
    @Id
    @GeneratedValue
    @Column
    private Long id;
    private Long customerId;
    @Column(nullable = false/*, unique = true*/)
   private Long productId;
   @Enumerated(EnumType.STRING)
    private OrderStatusEnum status;
    private LocalDate orderDate;
	private LocalDate lastUpdateDate;
	private LocalDate takenDate;
	private LocalDate prepareDate;
	private LocalDate shippedDate;
	private LocalDate deliveredDate;
	private LocalDate cancelledDate;
	private LocalDate extraditionRequestDate;
	private LocalDate extraditionRequestShippedDate;
	private LocalDate extraditionRequestInvestigationDate;
	private LocalDate extraditionRequestCancelledDate;
	private LocalDate extraditionRequestApprovedDate;
	private Long lastUpdaterUserId;
    private Integer orderCount;
	private Long userId;
    public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public Long getCustomerId() {
		return customerId;
	}




	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}




	public Long getProductId() {
		return productId;
	}




	public void setProductId(Long productId) {
		this.productId = productId;
	}




	public OrderStatusEnum getStatus() {
		return status;
	}




	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}
	

	public LocalDate getOrderDate() {
		return orderDate;
	}




	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}




	public LocalDate getLastUpdateDate() {
		return lastUpdateDate;
	}




	public void setLastUpdateDate(LocalDate lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}




	public LocalDate getTakenDate() {
		return takenDate;
	}




	public void setTakenDate(LocalDate takenDate) {
		this.takenDate = takenDate;
	}




	public LocalDate getPrepareDate() {
		return prepareDate;
	}




	public void setPrepareDate(LocalDate prepareDate) {
		this.prepareDate = prepareDate;
	}




	public LocalDate getShippedDate() {
		return shippedDate;
	}




	public void setShippedDate(LocalDate shippedDate) {
		this.shippedDate = shippedDate;
	}




	public LocalDate getDeliveredDate() {
		return deliveredDate;
	}




	public void setDeliveredDate(LocalDate deliveredDate) {
		this.deliveredDate = deliveredDate;
	}




	public LocalDate getCancelledDate() {
		return cancelledDate;
	}




	public void setCancelledDate(LocalDate cancelledDate) {
		this.cancelledDate = cancelledDate;
	}




	public LocalDate getExtraditionRequestDate() {
		return extraditionRequestDate;
	}




	public void setExtraditionRequestDate(LocalDate extraditionRequestDate) {
		this.extraditionRequestDate = extraditionRequestDate;
	}




	public LocalDate getExtraditionRequestShippedDate() {
		return extraditionRequestShippedDate;
	}




	public void setExtraditionRequestShippedDate(LocalDate extraditionRequestShippedDate) {
		this.extraditionRequestShippedDate = extraditionRequestShippedDate;
	}




	public LocalDate getExtraditionRequestInvestigationDate() {
		return extraditionRequestInvestigationDate;
	}




	public void setExtraditionRequestInvestigationDate(LocalDate extraditionRequestInvestigationDate) {
		this.extraditionRequestInvestigationDate = extraditionRequestInvestigationDate;
	}




	public LocalDate getExtraditionRequestCancelledDate() {
		return extraditionRequestCancelledDate;
	}




	public void setExtraditionRequestCancelledDate(LocalDate extraditionRequestCancelledDate) {
		this.extraditionRequestCancelledDate = extraditionRequestCancelledDate;
	}




	public LocalDate getExtraditionRequestApprovedDate() {
		return extraditionRequestApprovedDate;
	}




	public void setExtraditionRequestApprovedDate(LocalDate extraditionRequestApprovedDate) {
		this.extraditionRequestApprovedDate = extraditionRequestApprovedDate;
	}




	public Long getLastUpdaterUserId() {
		return lastUpdaterUserId;
	}




	public void setLastUpdaterUserId(Long lastUpdaterUserId) {
		this.lastUpdaterUserId = lastUpdaterUserId;
	}




	public Integer getOrderCount() {
		return orderCount;
	}




	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}




	public Long getUserId() {
		return userId;
	}




	public void setUserId(Long userId) {
		this.userId = userId;
	}

	



	public OrderDTO toDTO() {
    	OrderDTO orderDTO = new OrderDTO();
    	orderDTO.setId(getId());
    	orderDTO.setUserId(getUserId());
    	orderDTO.setCustomerId(getCustomerId());
    	orderDTO.setDeliveredDate(getDeliveredDate());
    	orderDTO.setExtraditionRequestApprovedDate(getExtraditionRequestApprovedDate());
    	orderDTO.setExtraditionRequestDate(getExtraditionRequestDate());
    	orderDTO.setOrderCount(getOrderCount());
    	orderDTO.setOrderDate(getOrderDate());
    	orderDTO.setProductId(getProductId());
    	orderDTO.setShippedDate(getShippedDate());
    	orderDTO.setStatus(getStatus());
    	orderDTO.setUserId(getId());    	
    	orderDTO.setLastUpdateDate(getLastUpdateDate());
    	orderDTO.setTakenDate(getTakenDate());
    	orderDTO.setPrepareDate(getPrepareDate());
    	orderDTO.setShippedDate(getShippedDate());
    	orderDTO.setDeliveredDate(getDeliveredDate());
    	orderDTO.setCancelledDate(getCancelledDate());
    	orderDTO.setExtraditionRequestDate(getExtraditionRequestDate());
    	orderDTO.setExtraditionRequestShippedDate(getExtraditionRequestShippedDate());;
    	orderDTO.setExtraditionRequestInvestigationDate(getExtraditionRequestInvestigationDate());;
    	orderDTO.setExtraditionRequestCancelledDate(getExtraditionRequestCancelledDate());
    	orderDTO.setExtraditionRequestApprovedDate(getExtraditionRequestApprovedDate());
    	return orderDTO;
    }



}
