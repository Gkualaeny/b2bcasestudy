package com.ordermanagement.ordermanagement.enums;

public enum OrderStatusEnum {
	//yeni bir statu eklenmesi durumunda order tablosuna statu date i eklemeyi unutmayiniz.ornek  statu taken eklenmisse takenDate olarak order tablosuna ekleyiniz.
TAKEN,
PREPARE,
SHIPPED,
DELIVERED,
CANCELLED,
EXTRADITION_REQUEST,
EXTRADITION_REQUEST_SHIPPED,
EXTRADITION_REQUEST_INVESTIGATION,
EXTRADITION_REQUEST_CANCELLED,
EXTRADITION_REQUEST_APPROVED;
}
