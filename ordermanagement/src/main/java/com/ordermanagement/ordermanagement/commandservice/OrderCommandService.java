package com.ordermanagement.ordermanagement.commandservice;

import java.lang.reflect.Field;
import java.time.LocalDate;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ordermanagement.ordermanagement.constant.Constants;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.dto.OrderingDTO;
import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;
import com.ordermanagement.ordermanagement.enums.ExceptionEnum;
import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;
import com.ordermanagement.ordermanagement.model.Order;
import com.ordermanagement.ordermanagement.repository.OrderRepository;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;


@Service
public class OrderCommandService implements IOrderCommandService {
	private OrderRepository orderRepository;
	@Autowired
	public OrderCommandService(OrderRepository orderRepository) {
		this.orderRepository=orderRepository;
	}

	@Override
	public OrderDTO ordering (OrderingDTO orderingDTO) throws Exception {
		Boolean validationCondition=orderingDTO.getOrderDate().compareTo(LocalDate.now())<=0;
		//sipris tarihi ileri bir tarih secilmelidir.
		ApplicationUtils.throwBusinessException(validationCondition, ExceptionEnum.ORDER_DATE_NOT_PAST);
		//sipariş sayisi girilmisse ve birden kucuk bir deger ise update islemi kabul edilmez null bir değer ise bu alan guncellenmez
		validationCondition=orderingDTO.getOrderCount()!=null && orderingDTO.getOrderCount()<1;
		ApplicationUtils.throwBusinessException(validationCondition, ExceptionEnum.ORDER_COUNT_NOT_LOWER_THAN_ONE);
		Order order=new Order();
		order.setUserId(orderingDTO.getUserId());
		//verilen statuye gore o statu ait olan date kolonu otomatik setlenmektedir.
		setDateNowAccordingToStatus(OrderStatusEnum.TAKEN,order);
		order.setStatus(OrderStatusEnum.TAKEN);
		order.setProductId(orderingDTO.getProductId());
		order.setOrderDate(orderingDTO.getOrderDate());
		order.setOrderCount(orderingDTO.getOrderCount());
		order.setUserId(orderingDTO.getUserId());
		order.setLastUpdateDate(LocalDate.now());
		order.setLastUpdaterUserId(orderingDTO.getUserId());
		order.setCustomerId(orderingDTO.getCustomerId());
		Order result=orderRepository.save(order);
		return result.toDTO();
	}
	//Verilen statuye gore reflection ile ilgili statunun hangi tarihte verildigini belirlemek amaci ile verilen staunun ilgili kolonun bulur 
	private void setDateNowAccordingToStatus(OrderStatusEnum status, Order order) {
		String concateText=ApplicationUtils.concater(ApplicationUtils.getCamelCase(status.toString()),Constants.DATE);
		Class<Order> clazz = Order.class;	
		Field field;
		try {
			field = clazz.getDeclaredField(concateText);
			field.setAccessible(true);
			field.set(order, LocalDate.now());
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	
	}

	@Override
	public OrderDTO updateOrder (UpdateOrderDTO updateOrderDTO ) throws Exception {
		Order order=orderRepository.getOne(updateOrderDTO.getId());
			/**business validation begin**/
			//sipariş tarihi alanı guncellenecekse ileri bir tarih olmaidir.
			Boolean validationCondition=updateOrderDTO.getOrderDate()!=null && updateOrderDTO.getOrderDate().compareTo(LocalDate.now())<=0;
			ApplicationUtils.throwBusinessException(validationCondition, ExceptionEnum.ORDER_DATE_NOT_PAST);
			//sipariş sayisi girilmisse ve birden kucuk bir deger ise update islemi kabul edilmez null bir değer ise bu alan guncellenmez
			validationCondition=updateOrderDTO.getOrderCount()!=null && updateOrderDTO.getOrderCount()<1;
			ApplicationUtils.throwBusinessException(validationCondition, ExceptionEnum.ORDER_COUNT_NOT_LOWER_THAN_ONE);
			/**business validation end**/
			order.setLastUpdaterUserId(updateOrderDTO.getUserId());
			if(updateOrderDTO.getStatus()!=null) {
				//verilen statuye gore o statu ait olan date kolonu otomatik setlenmektedir.
				setDateNowAccordingToStatus(updateOrderDTO.getStatus(),order);
				order.setStatus(updateOrderDTO.getStatus());	
			}
			order.setProductId(updateOrderDTO.getProductId()!=null?updateOrderDTO.getProductId():order.getProductId());
			order.setOrderDate(updateOrderDTO.getOrderDate()!=null?updateOrderDTO.getOrderDate():order.getOrderDate());
			order.setLastUpdateDate(LocalDate.now());
			order.setLastUpdaterUserId(updateOrderDTO.getUserId());
			order.setCustomerId(updateOrderDTO.getCustomerId()!=null?updateOrderDTO.getCustomerId():order.getCustomerId());
			Order result=orderRepository.save(order);
			order.setId(result.getId());
		return order.toDTO();
	}
	@Override
	public void deleteOrder(Long id) throws Exception {
		if (id != null) {
			Order order = orderRepository.getOne(id);
				orderRepository.delete(order);
		}
	}
}
