package com.ordermanagement.ordermanagement.commandservice;

import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.dto.OrderingDTO;
import com.ordermanagement.ordermanagement.dto.UpdateOrderDTO;

public interface IOrderCommandService {

	public OrderDTO ordering(OrderingDTO orderingDTO) throws Exception;

	public OrderDTO updateOrder(UpdateOrderDTO updateOrderDTO) throws Exception;

	public void deleteOrder(Long id) throws Exception;

}
