package com.ordermanagement.ordermanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ordermanagement.ordermanagement.commandservice.OrderCommandService;
import com.ordermanagement.ordermanagement.dto.OrderDTO;
import com.ordermanagement.ordermanagement.queryService.OrderQueryService;
import com.ordermanagement.ordermanagement.request.RequestDeleteOrder;
import com.ordermanagement.ordermanagement.request.RequestOrdering;
import com.ordermanagement.ordermanagement.request.RequestUpdateOrder;
import com.ordermanagement.ordermanagement.response.ResponseOrder;
import com.ordermanagement.ordermanagement.util.ApplicationUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/orderapi")
@Api(value = "orderapi")
public class OrderController {
	private OrderCommandService orderCommandService;
	private OrderQueryService orderQueryService;

	@Autowired
	public OrderController(OrderCommandService orderCommandService,OrderQueryService orderQueryService) {
		this.orderCommandService=orderCommandService;
		this.orderQueryService=orderQueryService;

	}
	/**/
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/get/order/by/orderId/{orderId}")
	public ResponseOrder getOrderByOrderId(@PathVariable @NotNull Long orderId) {
		ResponseOrder response=new ResponseOrder();
		OrderDTO orderDTO=orderQueryService.getOrderByOrderId(orderId);
		if(orderDTO!=null){
			List<OrderDTO> resultList=new ArrayList<>();
			resultList.add(orderDTO);
			response.setOrderDTOList(resultList);		
		}
		response.responseSuccessSetter();
		return response;
	}
	@ResponseStatus(HttpStatus.OK)
	@GetMapping("/get/order/by/user/id/{userId}")
	public ResponseOrder getOrdersByUserId(@PathVariable @NotNull Long userId) {
		ResponseOrder response=new ResponseOrder();
		List<OrderDTO> orderDTOs=orderQueryService.getOrdersByUserId(userId);
		if(!ApplicationUtils.isListEmptyOrNull(orderDTOs)) {
			response.setOrderDTOList(orderDTOs);		
		}
		response.responseSuccessSetter();
		return response;
	}

	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "", notes = "Get data of application screens")
	@DeleteMapping( value="delete/order", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseOrder deleteOrder(@RequestBody @Valid @NotNull RequestDeleteOrder request) throws Exception {
		orderCommandService.deleteOrder(request.getOrder().getId());
		ResponseOrder response=new ResponseOrder();
		response.setSuccess(true);
        response.responseSuccessSetter();
		return response;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "", notes = "Get data of application screens")
	@PostMapping( value="update/order", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseOrder updateOrder(@RequestBody @Valid @NotNull RequestUpdateOrder request) throws Exception {
		OrderDTO orderDTO = orderCommandService.updateOrder(request.getUpdateOrderDTO());
		ResponseOrder response=new ResponseOrder();
		List<OrderDTO> resultList=new ArrayList<>();
		resultList.add(orderDTO);
		response.setOrderDTOList(resultList);
		response.responseSuccessSetter();
		return response;
	}
	
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "", notes = "Get data of application screens")
	@PostMapping( value="/ordering", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseOrder ordering(@RequestBody @Valid @NotNull RequestOrdering request) throws Exception {
		OrderDTO orderDTO = orderCommandService.ordering(request.getOrderingDTO());
		ResponseOrder response=new ResponseOrder();
		List<OrderDTO> resultList=new ArrayList<>();
		resultList.add(orderDTO);
		response.setOrderDTOList(resultList);
		response.responseSuccessSetter();
		return response;
	}

}
