/**
 *
 */
package com.ordermanagement.ordermanagement.constant;

/**
 * @author burak.torgay
 *
 */
public class Constants {

    public static final String SET = "set";
    public static final String PROCESS_SUCCESS = "İşleminiz başırı ile gerçekleştirilmiştir.";
    public static final String PROCESS_SUCCESS_CODE = "OK";
	public static final String DATE = "Date";
    
    
    private Constants() {
        // Private Constructor
    }
}
