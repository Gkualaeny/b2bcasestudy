package com.ordermanagement.ordermanagement.response;

import java.util.List;

import com.ordermanagement.ordermanagement.dto.OrderDTO;

public class ResponseOrder extends BaseApiResponse{

	private List<OrderDTO> orderDTOList;

	public List<OrderDTO> getOrderDTOList() {
		return orderDTOList;
	}

	public void setOrderDTOList(List<OrderDTO> orderDTOList) {
		this.orderDTOList = orderDTOList;
	}






}
