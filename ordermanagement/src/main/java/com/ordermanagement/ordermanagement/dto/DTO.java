package com.ordermanagement.ordermanagement.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@NotNull
@Valid
public class DTO {
	@NotNull
	@Valid
    private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
