package com.ordermanagement.ordermanagement.dto;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class OrderingDTO {
	@NotNull
	@Valid
	private Long customerId;
	@NotNull
	@Valid
	private Long productId;
	@NotNull
	@Valid
	private Integer orderCount;
	@NotNull
	@Valid
	private LocalDate orderDate;
	@NotNull
	@Valid
	private Long userId;
	@NotNull
	@Valid
	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
