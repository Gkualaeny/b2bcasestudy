package com.ordermanagement.ordermanagement.dto;

import java.time.LocalDate;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;

public class UpdateOrderDTO {
	@NotNull
	@Valid
	private Long id;
	@NotNull
	@Valid
	private Long customerId;
	@NotNull
	@Valid
	private Long productId;
	@NotNull
	@Valid
	@Size(min = 1, message 
    = "Sipariş sayınız 1 den küçük olamaz.")
	private Integer orderCount;
	@NotNull
	@Valid
	private LocalDate orderDate;
	@NotNull
	@Valid
	private OrderStatusEnum status;
	@NotNull
	@Valid
	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public LocalDate getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(LocalDate orderDate) {
		this.orderDate = orderDate;
	}

	public OrderStatusEnum getStatus() {
		return status;
	}

	public void setStatus(OrderStatusEnum status) {
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

}
