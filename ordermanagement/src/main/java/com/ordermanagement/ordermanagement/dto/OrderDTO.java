package com.ordermanagement.ordermanagement.dto;

import java.time.LocalDate;

import com.ordermanagement.ordermanagement.enums.OrderStatusEnum;

public class OrderDTO extends DTO{

	    private Long customerId;
	    private Long productId;
	    private OrderStatusEnum status;
	    private Integer orderCount;
		private Long lastUpdaterUserId;
	    private LocalDate orderDate;
		private LocalDate lastUpdateDate;
		private LocalDate takenDate;
		private LocalDate prepareDate;
		private LocalDate shippedDate;
		private LocalDate deliveredDate;
		private LocalDate cancelledDate;
		private LocalDate extraditionRequestDate;
		private LocalDate extraditionRequestShippedDate;
		private LocalDate extraditionRequestInvestigationDate;
		private LocalDate extraditionRequestCancelledDate;
		private LocalDate extraditionRequestApprovedDate;
	    private Long userId;

		public Long getCustomerId() {
			return customerId;
		}
		public void setCustomerId(Long customerId) {
			this.customerId = customerId;
		}
		public Long getProductId() {
			return productId;
		}
		public void setProductId(Long productId) {
			this.productId = productId;
		}
		public OrderStatusEnum getStatus() {
			return status;
		}
		public void setStatus(OrderStatusEnum status) {
			this.status = status;
		}

		public Long getLastUpdaterUserId() {
			return lastUpdaterUserId;
		}
		public void setLastUpdaterUserId(Long lastUpdaterUserId) {
			this.lastUpdaterUserId = lastUpdaterUserId;
		}
		public LocalDate getTakenDate() {
			return takenDate;
		}
		public void setTakenDate(LocalDate takenDate) {
			this.takenDate = takenDate;
		}
		public LocalDate getPrepareDate() {
			return prepareDate;
		}
		public void setPrepareDate(LocalDate prepareDate) {
			this.prepareDate = prepareDate;
		}
		public LocalDate getCancelledDate() {
			return cancelledDate;
		}
		public void setCancelledDate(LocalDate cancelledDate) {
			this.cancelledDate = cancelledDate;
		}
		public LocalDate getExtraditionRequestInvestigationDate() {
			return extraditionRequestInvestigationDate;
		}
		public void setExtraditionRequestInvestigationDate(LocalDate extraditionRequestInvestigationDate) {
			this.extraditionRequestInvestigationDate = extraditionRequestInvestigationDate;
		}
		public LocalDate getExtraditionRequestCancelledDate() {
			return extraditionRequestCancelledDate;
		}
		public void setExtraditionRequestCancelledDate(LocalDate extraditionRequestCancelledDate) {
			this.extraditionRequestCancelledDate = extraditionRequestCancelledDate;
		}
		public LocalDate getOrderDate() {
			return orderDate;
		}
		public void setOrderDate(LocalDate orderDate) {
			this.orderDate = orderDate;
		}
		public LocalDate getDeliveredDate() {
			return deliveredDate;
		}
		public void setDeliveredDate(LocalDate deliveredDate) {
			this.deliveredDate = deliveredDate;
		}
		public Integer getOrderCount() {
			return orderCount;
		}
		public void setOrderCount(Integer orderCount) {
			this.orderCount = orderCount;
		}

		public LocalDate getExtraditionRequestApprovedDate() {
			return extraditionRequestApprovedDate;
		}
		public void setExtraditionRequestApprovedDate(LocalDate extraditionRequestApprovedDate) {
			this.extraditionRequestApprovedDate = extraditionRequestApprovedDate;
		}
		public LocalDate getExtraditionRequestDate() {
			return extraditionRequestDate;
		}
		public void setExtraditionRequestDate(LocalDate extraditionRequestDate) {
			this.extraditionRequestDate = extraditionRequestDate;
		}
		public LocalDate getExtraditionRequestShippedDate() {
			return extraditionRequestShippedDate;
		}
		public void setExtraditionRequestShippedDate(LocalDate extraditionRequestShippedDate) {
			this.extraditionRequestShippedDate = extraditionRequestShippedDate;
		}
		public Long getUserId() {
			return userId;
		}
		public void setUserId(Long userId) {
			this.userId = userId;
		}
		public LocalDate getShippedDate() {
			return shippedDate;
		}
		public void setShippedDate(LocalDate shippedDate) {
			this.shippedDate = shippedDate;
		}

		public LocalDate getLastUpdateDate() {
			return lastUpdateDate;
		}
		public void setLastUpdateDate(LocalDate lastUpdateDate) {
			this.lastUpdateDate = lastUpdateDate;
		}
}
