package com.ordermanagement.ordermanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ordermanagement.ordermanagement.model.Order;

public interface OrderRepository  extends JpaRepository<Order, Long> {


	List<Order> findByUserId(Long userId);

}
